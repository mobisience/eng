(function () {
  'use strict';

  angular
    .module('frontend')
    .controller('ManageController', ManageController);

  /** @ngInject */
  function ManageController(DecksFactory, $cookies, MeFactory, $mdToast, FollowsFactory, $location) {
    var vm = this;

    vm.newDeck = {};
    vm.decks = [];

    vm.clickSave = clickSave;
    vm.go = go;

    checkToken();
    loadDecks();

    function checkToken() {
      if (!$cookies.get('token')) {
        go('/home');
      }
    }

    function clickSave() {
      if (vm.newDeck.name && vm.newDeck.description) {
        DecksFactory.add({}, vm.newDeck, function (data) {
          showSimpleToast('Added new deck');
          loadDecks();
        });
      }
    }

    function showSimpleToast(text) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(text)
          .position('bottom right')
          .hideDelay(3000)
      );
    }

    function loadDecks() {
      MeFactory.get({resource: 'decks'}, {}, function (data) {
        vm.decks = data.map(function (item, index) {
          FollowsFactory.getCount({resource: 'decks', id: item.id, value: 'count'}, {}, function (data) {
            item.follows = data.count;
          });
          return item;
        });
      })
    }

    function go(path) {
      $location.path(path);
    }
  }
})();
