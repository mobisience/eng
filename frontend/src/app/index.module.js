(function () {
  'use strict';

  angular
    .module('frontend', ['ngResource', 'ngRoute', 'toastr', 'ngCookies', 'ngAnimate', 'ngMaterial']);
})();
