(function () {
  'use strict';

  angular
    .module('frontend')
    .controller('MyCardsController', MyCardsController);

  /** @ngInject */
  function MyCardsController($location, $cookies, MeFactory, DecksFactory, FollowsFactory, CardsFactory, ResultsFactory) {
    var vm = this;

    vm.follows = [];

    vm.startLearn = startLearn;

    checkToken();
    loadFollows();

    function startLearn(follow) {
      go('/learn/' + follow.id);
    }

    function checkToken() {
      if (!$cookies.get('token')) {
        go('/home');
      }
    }

    function loadFollows() {
      MeFactory.get({resource: 'follows'}, {}, function (data) {
        vm.follows = data.map(function (item, index) {
          DecksFactory.getOne({id: item.deck.id}, {}, function (data) {
            item.author = data.account.name + ' ' + data.account.surname;
          });
          FollowsFactory.getCount({resource: 'decks', id: item.deck.id, value: 'count'}, {}, function (data) {
            item.follows = data.count;
          });
          CardsFactory.getCountNewByDeck({id: item.deck.id}, {}, function (data) {
            item.new = data.count;
          });
          CardsFactory.getCountByDeck({id: item.deck.id}, {}, function (data) {
            item.all = data.count;
          });
          ResultsFactory.getCountRevByDeck({id: item.deck.id}, {}, function (data) {
            item.rev = data.count;
          });
          return item;
        });
      })
    }

    function go(path) {
      $location.path(path);
    }
  }
})();
