(function () {
  'use strict';

  angular
    .module('frontend')
    .config(config)
    .factory('sessionInjector', function ($cookies) {
      return {
        request: function (config) {
          if ($cookies.get('token')) {
            config.headers['Authorization'] = $cookies.get('token');
          }
          return config;
        }
      };
    });

  /** @ngInject */
  function config($locationProvider, $logProvider, toastrConfig, $httpProvider, $mdThemingProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
    $httpProvider.interceptors.push('sessionInjector');
  }

})();
