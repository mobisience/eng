(function () {
  'use strict';
  angular
    .module('frontend')
    .factory("FollowsFactory", FollowsFactory);

  function FollowsFactory($resource, followsAPI) {
    return $resource(followsAPI  + ':resource/:id/:value', {
      resource: '@resource',
      id: '@id',
      value: '@value'
    }, {
      getOne:{
        url:followsAPI  + ':id',
        method: 'GET',
        isArray:false
      },
      getCount:{
        method: 'GET',
        isArray:false
      },
      insert:{
        method: 'POST',
        isArray:false
      },
      patch:{
        url:followsAPI  + ':id',
        method: 'PATCH',
        isArray:false
      },
      delete:{
        url: followsAPI + ':id',
        method: 'DELETE'
      }
    });

  }
})();
