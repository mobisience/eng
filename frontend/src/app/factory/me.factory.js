(function () {
  'use strict';
  angular
    .module('frontend')
    .factory("MeFactory", MeFactory);

  function MeFactory($resource, meAPI) {
    return $resource(meAPI + ':resource/:page/:id', {
      resource: '@resource',
      page: '@page',
      id: '@id'
    }, {
      get: {
        method: 'GET',
        isArray: true
      },
      getNonArray: {
        method: 'GET',
        isArray: false
      },
      getMyFollowsByDeck: {
        url: meAPI + 'follows/decks/:id',
        method: 'GET',
        isArray: false
      }
    });

  }
})();
