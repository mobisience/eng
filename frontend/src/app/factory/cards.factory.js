(function () {
  'use strict';
  angular
    .module('frontend')
    .factory("CardsFactory", CardsFactory);

  function CardsFactory($resource, cardsAPI) {
    return $resource(cardsAPI + ':resource/:id/:value', {
      resource: '@resource',
      id: '@id',
      value: '@value'
    }, {
      getCountByDeck: {
        url:cardsAPI + 'decks/:id/count',
        method: 'GET',
        isArray:false
      },
      getCountNewByDeck: {
        url:cardsAPI + 'decks/:id/new/count',
        method: 'GET',
        isArray:false
      },
      get: {
        method: 'GET',
        isArray: true
      },
      insert: {
        method: 'POST',
        isArray: false
      },
      delete: {
        url: cardsAPI + ':id',
        method: 'DELETE'
      }
    });

  }
})();
