(function () {
  'use strict';
  angular
    .module('frontend')
    .factory("DecksFactory", DecksFactory);

  function DecksFactory($resource, decksAPI) {
    return $resource(decksAPI + ':id' , {
      id: '@id'
    }, {
      add: {
        method: 'POST',
        isArray: false
      },
      put: {
        method: 'PUT',
        isArray: false
      },
      get:{
        method: 'GET',
        isArray:true
      },
      getOne:{
        method: 'GET',
        isArray:false
      },
      remove:{
        method: 'DELETE'
      }
    });

  }
})();
