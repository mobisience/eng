(function () {
  'use strict';
  angular
    .module('frontend')
    .factory("ResultsFactory", ResultsFactory);

  function ResultsFactory($resource, resultsAPI) {
    return $resource(resultsAPI + ':resource/:id/:value', {
      resource: '@resource',
      id: '@id',
      value: '@value'
    }, {
      getCountRevByDeck: {
        url:resultsAPI + 'decks/:id/rev/count',
        method: 'GET',
        isArray:false
      },
      get: {
        method: 'GET',
        isArray: true
      },
      delete: {
        url: resultsAPI + ':id',
        method: 'DELETE'
      },
      next: {
        url: resultsAPI + 'decks/:id/next',
        method: 'POST',
        isArray: false
      }
    });

  }
})();
