(function () {
  'use strict';
  angular
    .module('frontend')
    .factory("WordsFactory", WordsFactory);

  function WordsFactory($resource, wordsAPI) {
    return $resource(wordsAPI + ':resource/:id/:value', {
      resource: '@resource',
      id: '@id',
      value: '@value'
    }, {
      get: {
        method: 'GET',
        isArray: true
      },
      put: {
        url: wordsAPI + ':id',
        method: 'PUT',
        isArray: false
      },
      insert: {
        method: 'POST',
        isArray: false
      },
      delete: {
        url: wordsAPI + ':id',
        method: 'DELETE'
      }
    });

  }
})();
