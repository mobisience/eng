(function () {
  'use strict';

  angular
    .module('frontend')
    .controller('LearnController', LearnController);

  /** @ngInject */
  function LearnController($location, $cookies, $mdToast, $routeParams, MeFactory, FollowsFactory, CardsFactory, ResultsFactory) {
    var vm = this;

    var followId = $routeParams.follow;
    var account = {};
    var follow = {};
    var deckId = -1;
    var changeWords = false;

    vm.currentWords = {fword:{}, sword:{}};
    vm.showSWord = false;

    vm.currentWords.fword.word = '?';
    vm.currentWords.sword.word = '?';

    vm.clickResult = clickResult;
    vm.clickSWord = clickSWord;
    vm.clickSwapWords = clickSwapWords;

    checkToken();
    checkAuthorization();

    function checkToken() {
      if (!$cookies.get('token')) {
        go('/home');
      }
    }

    function checkAuthorization() {
      MeFactory.getNonArray({resource: 'accounts'}, {}, function (data) {
        account = data;
        FollowsFactory.getOne({id: followId}, {}, function (data) {
          follow = data;
          if (follow.id == undefined || follow.user.id != account.id) {
            go('/myCards');
          }
          changeWords = follow.startWithFWord;
          deckId = follow.deck.id;
          nextWord(null);
        })
      })
    }

    function clickSwapWords() {
      follow.startWithFWord = !follow.startWithFWord;
      FollowsFactory.patch({id: follow.id}, follow, function (data) {
        changeWords = !changeWords;
        showSimpleToast('changed words');
        nextWord(null);
      });
    }

    function clickResult(result) {
      //AGAIN, HARD, EASY
      if (!vm.showSWord) {
        vm.showSWord = true;
      } else{
        vm.currentWords.fword.word = 'Loading...';
        vm.currentWords.sword.word = 'Loading...';
        nextWord(result);
      }
    }

    function nextWord(result) {
      var responseBody = {};
      if(result != null){
        responseBody = {card: vm.currentWords, resultType: result};
      }

      console.log(responseBody);
      ResultsFactory.next({id: deckId}, responseBody, function(data){
        vm.currentWords = swapWords(data);
        vm.showSWord = false;
      });
    }


    function swapWords(item) {
      if(!changeWords){
        var temp = item.fword;
        item.fword = item.sword;
        item.sword = temp;
      }
      return item;
    }

    function showSimpleToast(text) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(text)
          .position('bottom right')
          .hideDelay(3000)
      );
    }

    function clickSWord() {
      vm.showSWord = true;
    }

    function go(path) {
      $location.path(path);
    }
  }
})();
