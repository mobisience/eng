(function () {
  'use strict';

  angular
    .module('frontend')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          tab: '@tab'
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($location, $scope) {
      var vm = this;
      vm.goto = goto;

      function goto(location) {
    	if(!vm.tab){
        	vm.tab=location;
    	}
        $location.path(location);
      }
    }
  }
})();
