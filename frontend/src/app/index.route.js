angular.module('frontend').config(routerConfig);

/** @ngInject */
function routerConfig($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl : 'app/main/main.html',
		controller : 'MainController',
		controllerAs : 'vm'
	}).when('/register', {
		templateUrl : 'app/register/register.html',
		controller : 'RegisterController',
		controllerAs : 'vm'
	}).when('/myCards', {
		templateUrl : 'app/my_cards/my_cards.html',
		controller : 'MyCardsController',
		controllerAs : 'vm'
	}).when('/learn/:follow', {
		templateUrl : 'app/learn/learn.html',
		controller : 'LearnController',
		controllerAs : 'vm'
	}).when('/edit/:deck', {
		templateUrl : 'app/edit_deck/edit_deck.html',
		controller : 'EditDeckController',
		controllerAs : 'vm'
	}).when('/manage', {
		templateUrl : 'app/manage/manage.html',
		controller : 'ManageController',
		controllerAs : 'vm'
	}).when('/browse', {
		templateUrl : 'app/browse/browse.html',
		controller : 'BrowseController',
		controllerAs : 'vm'
	}).otherwise('/home');
}
