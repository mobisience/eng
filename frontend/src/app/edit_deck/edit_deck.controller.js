(function () {
  'use strict';

  angular
    .module('frontend')
    .controller('EditDeckController', EditDeckController);

  /** @ngInject */
  function EditDeckController($location, $cookies, $routeParams, CardsFactory, DecksFactory, MeFactory, WordsFactory, $mdToast) {
    var vm = this;

    var deckId = $routeParams.deck;

    vm.lang = ['EN', 'PL'];
    vm.selectedLangEdit = {};
    vm.selectedLangNew = {};
    vm.deck = {};
    vm.items = [];
    vm.edit = {};
    vm.newCard = {};

    vm.clickCard = clickCard;
    vm.editDeckName = editDeckName;
    vm.addNewCard = addNewCard;
    vm.updateCard = updateCard;

    checkToken();
    loadDeck(deckId);

    var DynamicItems = function () {
      getItems();
    };

    DynamicItems.prototype.getItemAtIndex = function (index) {
      return vm.items[index];
    };

    DynamicItems.prototype.getLength = function () {
      return vm.items.length;
    };

    vm.dynamicItems = new DynamicItems();


    function checkToken() {
      if (!$cookies.get('token')) {
        go('/home');
      }
    }

    function getItems() {
      CardsFactory.get({resource: 'decks', id: deckId}, {}, function (data) {
        vm.items = data;
      });
    }

    function clickCard(card) {
      vm.edit = card;
      vm.selectedLangEdit.fWord = card.fWord.lang;
      vm.selectedLangEdit.sWord = card.sWord.lang;
    }

    function loadDeck(deckId) {
      DecksFactory.getOne({id: deckId}, {}, function (data) {
        vm.deck = data;
        checkAuthorization();
      })
    }

    function editDeckName() {
      DecksFactory.put({id: vm.deck.id}, vm.deck, function (data) {
        showSimpleToast("Changed deck name");
      })
    }

    function updateCard() {
      WordsFactory.put({id:vm.edit.fWord.id}, {lang: vm.selectedLangEdit.fWord, word: vm.edit.fWord.word}, function (data) {
        WordsFactory.put({id:vm.edit.sWord.id}, {lang: vm.selectedLangEdit.sWord, word: vm.edit.sWord.word}, function (data) {
          showSimpleToast('Edited card');
          getItems();
        });
      });
    }

    function addNewCard() {
      var wordsId = {fWord: -1, sWord: -1};

      WordsFactory.insert({}, {lang: vm.selectedLangNew.fWord, word: vm.newCard.fWord}, function (data) {
        wordsId.fWord = data.id;

        WordsFactory.insert({}, {lang: vm.selectedLangNew.sWord, word: vm.newCard.sWord}, function (data) {
          wordsId.sWord = data.id;

          CardsFactory.insert({}, {
            deck: {id: vm.deck.id},
            fWord: {id: wordsId.fWord},
            sWord: {id: wordsId.sWord}
          }, function (data) {
            showSimpleToast('Added card');
            getItems();
          })
        });
      });
    }

    function showSimpleToast(text) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(text)
          .position('bottom right')
          .hideDelay(3000)
      );
    }

    function checkAuthorization() {
      MeFactory.getNonArray({resource: 'accounts'}, {}, function (data) {
        if (data.id != vm.deck.account.id) {
          go('/manage');
        }
      });
    }

    function go(path) {
      $location.path(path);
    }
  }
})();
