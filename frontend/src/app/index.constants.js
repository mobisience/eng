/* global malarkey:false, moment:false */
(function () {
  'use strict';

  angular
    .module('frontend')
    .constant('malarkey', malarkey)
    .constant('meAPI', '/api/me/')
    .constant('cardsAPI', '/api/cards/')
    .constant('accountsAPI', '/api/accounts/')
    .constant('decksAPI', '/api/decks/')
    .constant('followsAPI', '/api/follows/')
    .constant('resultsAPI', '/api/results/')
    .constant('wordsAPI', '/api/words/')
    .constant('loginAPI', '/api/login/')
    .constant('moment', moment);
})();
