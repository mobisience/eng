(function () {
  'use strict';

  angular
    .module('frontend')
    .controller('BrowseController', BrowseController);

  /** @ngInject */
  function BrowseController($cookies, $location, DecksFactory, CardsFactory, MeFactory, FollowsFactory, $mdToast) {
    var vm = this;

    var message = [{type: "md-warn", name: "Delete"}, {type: "", name: "Follow"}, {type: "", name: "Unfollow"}];
    vm.decks = [];

    vm.getButtonMessage = getButtonMessage;
    vm.clickButton = clickButton;

    checkToken();
    loadDecks();

    function checkToken() {
      if (!$cookies.get('token')) {
        go('/home');
      }
    }

    function clickButton(deck) {
      if (deck.your) {
        DecksFactory.remove({id: deck.id}, {}, function () {
          showSimpleToast('Deleted deck');
          loadDecks();
        });
      } else {
        if (deck.follow) {
          MeFactory.getMyFollowsByDeck({id: deck.id}, {}, function (data) {
            FollowsFactory.delete({}, {id: data.id}, function (data) {
              showSimpleToast('Unfollowed');
            })
          });
        } else {
          FollowsFactory.insert({}, {deck: {id: deck.id}, startWithFWord: true}, function (data) {
            showSimpleToast('Follow deck');
          })
        }
        loadDecks();
      }
    }

    function getButtonMessage(deck) {
      if (deck.your) {
        return message[0];
      } else {
        return (deck.follow) ? message[2] : message[1];
      }
    }

    function loadDecks() {
      var account = {};

      MeFactory.getNonArray({resource: 'accounts'}, {}, function (data) {
        account = data;
        DecksFactory.get({}, {}, function (data) {
          vm.decks = data.map(function (item, index) {
            item.your = (item.account.id == account.id) ? true : false;
            MeFactory.getMyFollowsByDeck({id: item.id}, {}, function (data) {
              item.follow = (data.id) ? true : false;
            });
            FollowsFactory.getCount({resource: 'decks', id: item.id, value: 'count'}, {}, function (data) {
              item.follows = data.count;
            });
            CardsFactory.getCountByDeck({id: item.id}, {}, function (data) {
              item.all = data.count;
            });
            return item;
          });
        });
      });
    }

    function showSimpleToast(text) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(text)
          .position('bottom right')
          .hideDelay(3000)
      );
    }

    function go(path) {
      $location.path(path);
    }
  }
})();
