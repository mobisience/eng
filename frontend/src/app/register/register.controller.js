(function () {
  'use strict';

  angular
    .module('frontend')
    .controller('RegisterController', RegisterController);

  /** @ngInject */
  function RegisterController(AccountsFactory, $location) {
    var vm = this;

    vm.clickRegister = clickRegister;
    vm.go = go;

    vm.user = {};

    function clickRegister() {
      if(vm.user.pass.first == vm.user.pass.second){
        vm.user.pass = vm.user.pass.first;
        AccountsFactory.register({}, vm.user, function (data) {
          go('/home');
        });
      }
    }

    function go(path) {
      $location.path(path);
    }
  }
})();
