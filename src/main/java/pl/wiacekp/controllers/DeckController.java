package pl.wiacekp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import pl.wiacekp.domain.Deck;
import pl.wiacekp.exceptions.DeckExceptions;
import pl.wiacekp.exceptions.FollowExceptions;
import pl.wiacekp.services.DeckService;
import pl.wiacekp.view.Views;

/**
 * Created by pwiacek on 10/11/2016.
 */
@RestController
@RequestMapping(value = "/api/decks")
public class DeckController {
    private DeckService deckService;

    @Autowired
    public DeckController(DeckService deckService) {
        this.deckService = deckService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @JsonView(Views.Decks.class)
    public ResponseEntity<Iterable<Deck>> getDecks(@RequestParam(value = "page", required = false) Integer page,
    		@RequestParam(value = "page", required = false) Integer size){
        return new ResponseEntity<>(deckService.getDecks(page, size), HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/{deckId}")
    @JsonView(Views.Decks.class)
    public ResponseEntity<Deck> getDeckById(@PathVariable Long deckId){
        return new ResponseEntity<>(deckService.getOne(deckId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    @JsonView(Views.Decks.class)
    public ResponseEntity<Deck> insertDeck(@RequestBody Deck deck){
        return new ResponseEntity<>(deckService.addDeck(deck), HttpStatus.OK);
    }
    

    @RequestMapping(method = RequestMethod.PUT, value = "/{deckId}")		
    @JsonView(Views.Decks.class)
    public ResponseEntity<Deck> updateDeck(@RequestBody Deck deck, @PathVariable Long deckId) throws DeckExceptions{			
        return new ResponseEntity<>(deckService.updateDeck(deck, deckId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{deckId}")
    public ResponseEntity<Object> deleteDeck(@PathVariable Long deckId) throws DeckExceptions, FollowExceptions {
        deckService.deleteDeck(deckId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
