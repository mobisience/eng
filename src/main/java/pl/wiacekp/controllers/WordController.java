package pl.wiacekp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import pl.wiacekp.domain.Word;
import pl.wiacekp.exceptions.WordExceptions;
import pl.wiacekp.services.WordService;
import pl.wiacekp.view.Views;

/**
 * Created by pwiacek on 10/11/2016.
 */
@RestController
@RequestMapping(value = "/api/words")
public class WordController {
    private WordService wordService;

    @Autowired
    public WordController(WordService wordService) {
        this.wordService = wordService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @JsonView(Views.Words.class)
    public ResponseEntity<Iterable<Word>> getWords(){
        return new ResponseEntity<>(wordService.getWords(), HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{wordId}")
    @JsonView(Views.Words.class)
    public ResponseEntity<Word> getWordById(@PathVariable Long wordId){
        return new ResponseEntity<>(wordService.getOne(wordId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    @JsonView(Views.Words.class)
    public ResponseEntity<Word> insertWord(@RequestBody Word word){
        return new ResponseEntity<>(wordService.addWord(word), HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.PUT, value = "/{wordId}")		
    @JsonView(Views.Words.class)
    public ResponseEntity<Word> updateWord(@RequestBody Word word, @PathVariable Long wordId) throws WordExceptions{			
        return new ResponseEntity<>(wordService.updateWord(word, wordId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{wordId}")
    public ResponseEntity<Object> deleteWord(@PathVariable Long wordId){
        wordService.deleteWord(wordId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
