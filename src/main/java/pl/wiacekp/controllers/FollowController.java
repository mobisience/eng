package pl.wiacekp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import pl.wiacekp.domain.Count;
import pl.wiacekp.domain.Follow;
import pl.wiacekp.exceptions.FollowExceptions;
import pl.wiacekp.services.FollowService;
import pl.wiacekp.view.Views;

/**
 * Created by pwiacek on 10/11/2016.
 */
@RestController
@RequestMapping(value = "/api/follows")
public class FollowController {
    private FollowService followService;

    @Autowired
    public FollowController(FollowService followService) {
        this.followService = followService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @JsonView(Views.Follows.class)
    public ResponseEntity<Iterable<Follow>> getFollows(){
        return new ResponseEntity<>(followService.getFollows(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value="/decks/{deckID}")
    @JsonView(Views.Follows.class)
    public ResponseEntity<Iterable<Follow>> getFollowsByDeck(@PathVariable Long deckID) throws FollowExceptions{
        return new ResponseEntity<>(followService.findAllByDeck(deckID), HttpStatus.OK);
    }
    

    @RequestMapping(method = RequestMethod.GET, value="/decks/{deckID}/count")
    @JsonView(Views.Follows.class)
    public ResponseEntity<Count> getFollowsCountByDeck(@PathVariable Long deckID) throws FollowExceptions{
        return new ResponseEntity<>(followService.getCountByDeck(deckID), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{followId}")
    @JsonView(Views.Follows.class)
    public ResponseEntity<Follow> getFollowById(@PathVariable Long followId){
        return new ResponseEntity<>(followService.getOne(followId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    @JsonView(Views.Follows.class)
    public ResponseEntity<Follow> insertFollow(@RequestBody Follow follow){
        return new ResponseEntity<>(followService.addFollow(follow), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PATCH, value="/{followId}")		
    @JsonView(Views.Follows.class)
    public ResponseEntity<Follow> updateFollow(@RequestBody Follow follow, @PathVariable Long followId) throws FollowExceptions{
        return new ResponseEntity<>(followService.updateFollow(follow, followId), HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.DELETE, value = "/{followId}")
    public ResponseEntity<Object> deleteFollow(@PathVariable Long followId){
        followService.deleteFollow(followId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
