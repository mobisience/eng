package pl.wiacekp.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.wiacekp.domain.Card;
import pl.wiacekp.domain.Count;
import pl.wiacekp.domain.Result;
import pl.wiacekp.exceptions.ResultExceptions;
import pl.wiacekp.services.ResultService;
import pl.wiacekp.view.Views;

import java.util.List;

/**
 * Created by pwiacek on 10/11/2016.
 */
@RestController
@RequestMapping(value = "/api/results")
public class ResultController {
    private ResultService resultService;

    @Autowired
    public ResultController(ResultService resultService) {
        this.resultService = resultService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @JsonView(Views.Results.class)
    public ResponseEntity<Iterable<Result>> getResults() {
        return new ResponseEntity<>(resultService.getResults(), HttpStatus.OK);
    }

    @RequestMapping(value = "/decks/{deckID}/rev", method = RequestMethod.GET)
    @JsonView(Views.Results.class)
    public ResponseEntity<List<Result>> getResultsRevByDeck(@PathVariable Long deckID) {
        return new ResponseEntity<>(resultService.getAllRevResultsByDeck(deckID), HttpStatus.OK);
    }

    @RequestMapping(value = "/decks/{deckID}/rev/count", method = RequestMethod.GET)
    @JsonView(Views.Results.class)
    public ResponseEntity<Count> getCountResultsRevByDeck(@PathVariable Long deckID) {
        return new ResponseEntity<>(resultService.getCountRevResultsByDeck(deckID), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{resultID}")
    @JsonView(Views.Results.class)
    public ResponseEntity<Result> getResultById(@PathVariable Long resultID) {
        return new ResponseEntity<>(resultService.getOne(resultID), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{resultId}")
    public ResponseEntity<Object> deleteResult(@PathVariable Long resultId) {
        resultService.deleteResult(resultId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/decks/{deckId}/next")//api/results/decks/-1/next
    @JsonView(Views.Cards.class)
    public ResponseEntity<Card> nextWord(@RequestBody Result result, @PathVariable Long deckId) throws ResultExceptions {
        return new ResponseEntity<Card>(resultService.nextCard(deckId, result), HttpStatus.OK);
    }
}
