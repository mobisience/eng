package pl.wiacekp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.annotation.JsonView;

import pl.wiacekp.domain.Account;
import pl.wiacekp.domain.Deck;
import pl.wiacekp.domain.Follow;
import pl.wiacekp.services.AccountService;
import pl.wiacekp.services.DeckService;
import pl.wiacekp.services.FollowService;
import pl.wiacekp.view.Views;

@RestController
@RequestMapping(value = "/api/me")
public class MeController {
    private DeckService deckService;
    private AccountService accountService;
    private FollowService followService;

    @Autowired
	public MeController(DeckService deckService, AccountService accountService, FollowService followService) {
		this.deckService = deckService;
		this.accountService = accountService;
		this.followService = followService;
	}


	@RequestMapping(value="/decks", method = RequestMethod.GET)
    @JsonView(Views.Decks.class)
    public ResponseEntity<Iterable<Deck>> getMyDecks(@RequestParam(value = "page", required = false) Integer page) {
        return new ResponseEntity<>(deckService.getDecksByOwner(), HttpStatus.OK);
    }
	

	@RequestMapping(value="/follows", method = RequestMethod.GET)
    @JsonView(Views.Follows.class)
    public ResponseEntity<Iterable<Follow>> getMyFollows(@RequestParam(value = "page", required = false) Integer page) {
        return new ResponseEntity<>(followService.getMyFollows(), HttpStatus.OK);
    }

	@RequestMapping(value="/follows/decks/{deckId}", method = RequestMethod.GET)
    @JsonView(Views.Follows.class)
    public ResponseEntity<Follow> getMyFollowsByDeck(@PathVariable Long deckId) {
        return new ResponseEntity<>(followService.getMyFollowsByDeck(deckId), HttpStatus.OK);
    }
	

	@RequestMapping(value="/accounts", method = RequestMethod.GET)
    @JsonView(Views.Accounts.class)
    public ResponseEntity<Account> getMyAccount() {		
        return new ResponseEntity<>(accountService.getMyAccount(), HttpStatus.OK);
    }
}
