package pl.wiacekp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import pl.wiacekp.domain.Card;
import pl.wiacekp.domain.Count;
import pl.wiacekp.exceptions.CardExceptions;
import pl.wiacekp.services.CardService;
import pl.wiacekp.view.Views;

/**
 * Created by pwiacek on 10/11/2016.
 */

@RestController
@RequestMapping(value = "/api/cards")
public class CardController {
    private CardService cardService;

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @JsonView(Views.Cards.class)
    public ResponseEntity<List<Card>> getCards(){
        return new ResponseEntity<>(cardService.getCards(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{cardId}")
    @JsonView(Views.Cards.class)
    public ResponseEntity<Card> getCardById(@PathVariable Long cardId){
        return new ResponseEntity<>(cardService.getOne(cardId), HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/decks/{deckId}")
    @JsonView(Views.Cards.class)
    public ResponseEntity<Iterable<Card>> getCardByDeckId(@PathVariable Long deckId){
        return new ResponseEntity<>(cardService.getCardsByDeckID(deckId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/decks/{deckId}/new")
    @JsonView(Views.Cards.class)
    public ResponseEntity<Iterable<Card>> getNewCardByDeckId(@PathVariable Long deckId) throws CardExceptions {
        return new ResponseEntity<>(cardService.findAllNewCardByUserAndDeck(deckId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/decks/{deckId}/new/count")
    @JsonView(Views.Cards.class)
    public ResponseEntity<Count> getCountNewCardByDeckId(@PathVariable Long deckId) throws CardExceptions {
        return new ResponseEntity<>(cardService.getCountNewCardByUserAndDeck(deckId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/decks/{deckId}/count")
    @JsonView(Views.Cards.class)
    public ResponseEntity<Count> getCountCardByDeckId(@PathVariable Long deckId) throws CardExceptions {
        return new ResponseEntity<>(cardService.getCountCardsByDeckId(deckId), HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.PUT, value = "/{cardId}")    
    @JsonView(Views.Cards.class)
    public ResponseEntity<Card> updateCard(@RequestBody Card card, @PathVariable Long cardId) throws CardExceptions{
        return new ResponseEntity<>(cardService.updateCard(cardId, card), HttpStatus.OK);
    }
    

    @RequestMapping(method = RequestMethod.POST)
    @JsonView(Views.Cards.class)
    public ResponseEntity<Card> insertCard(@RequestBody Card card){
        return new ResponseEntity<>(cardService.addCard(card), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{cardId}")
    public ResponseEntity<Object> deleteCard(@PathVariable Long cardId){
        cardService.deleteCard(cardId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
