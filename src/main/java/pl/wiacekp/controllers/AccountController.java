package pl.wiacekp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import pl.wiacekp.domain.Account;
import pl.wiacekp.services.AccountService;
import pl.wiacekp.view.Views;

/**
 * Created by pwiacek on 10/11/2016.
 */
@RestController
@RequestMapping(value = "/api/accounts")
public class AccountController {
    @Autowired
    AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    @JsonView(Views.Accounts.class)
    public ResponseEntity<Iterable<Account>> getUsers() {
        return new ResponseEntity<>(accountService.getAccounts(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}")
    @JsonView(Views.Accounts.class)
    public ResponseEntity<Account> getUserById(@PathVariable Long userId) {
        return new ResponseEntity<>(accountService.getOne(userId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    @JsonView(Views.Accounts.class)
    public ResponseEntity<Account> addUser(@RequestBody @Validated Account user) {
        return new ResponseEntity<>(accountService.addAccount(user), HttpStatus.OK);
    }
}
