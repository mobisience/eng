package pl.wiacekp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class CustomExceptionHandler {
    private final String unknownError = "UNKNOWN_ERROR";

    @ExceptionHandler(value = DeckExceptions.class)
    @ResponseBody
    public ResponseEntity<ApiResponseError> invitationException(DeckExceptions exception) {
        switch (exception.getFailReason()) {
            case UNAUTHORIZE:
                return getResponseEntity(401L, HttpStatus.UNAUTHORIZED, exception.getFailReason().toString());
            default:
                return getResponseEntity(400L, HttpStatus.BAD_REQUEST, unknownError);
        }
    }

    @ExceptionHandler(value = ResultExceptions.class)
    @ResponseBody
    public ResponseEntity<ApiResponseError> resultException(ResultExceptions exception) {
        switch (exception.getFailReason()) {
            case DECK_NOT_FOLLOWED:
            case UNAUTHORIZE:
                return getResponseEntity(401L, HttpStatus.UNAUTHORIZED, exception.getFailReason().toString());
            case DECK_NOT_FOUND:
            case CARD_NOT_FOUND:
                return getResponseEntity(404L, HttpStatus.NOT_FOUND, exception.getFailReason().toString());
            default:
                return getResponseEntity(400L, HttpStatus.BAD_REQUEST, unknownError);
        }
    }

    @ExceptionHandler(value = FollowExceptions.class)
    @ResponseBody
    public ResponseEntity<ApiResponseError> invitationException(FollowExceptions exception) {
        switch (exception.getFailReason()) {
        	case FOLLOW_NOT_FOUND:
            case DECK_NOT_FOUND:
                return getResponseEntity(404L, HttpStatus.NOT_FOUND, exception.getFailReason().toString());
            case UNAUTHORIZE:
                return getResponseEntity(401L, HttpStatus.UNAUTHORIZED, exception.getFailReason().toString());
            default:
                return getResponseEntity(400L, HttpStatus.BAD_REQUEST, unknownError);
        }
    }

    @ExceptionHandler(value = CardExceptions.class)
    @ResponseBody
    public ResponseEntity<ApiResponseError> invitationException(CardExceptions exception) {
        switch (exception.getFailReason()) {
            case DECK_NOT_FOUND:
                return getResponseEntity(404L, HttpStatus.NOT_FOUND, exception.getFailReason().toString());
            case UNAUTHORIZE:
                return getResponseEntity(401L, HttpStatus.UNAUTHORIZED, exception.getFailReason().toString());
            case PARAMETERS_NOT_CORRECT:
                return getResponseEntity(400L, HttpStatus.BAD_REQUEST, exception.getFailReason().toString());
            default:
                return getResponseEntity(400L, HttpStatus.BAD_REQUEST, unknownError);
        }
    }

    @ExceptionHandler(value = WordExceptions.class)
    @ResponseBody
    public ResponseEntity<ApiResponseError> invitationException(WordExceptions exception) {
        switch (exception.getFailReason()) {
            case UNATHORIZE:
                return getResponseEntity(401L, HttpStatus.UNAUTHORIZED, exception.getFailReason().toString());
            case CARD_NOT_FOUND:
                return getResponseEntity(403L, HttpStatus.NOT_FOUND, exception.getFailReason().toString());
            default:
                return getResponseEntity(400L, HttpStatus.BAD_REQUEST, unknownError);
        }
    }


    private ResponseEntity<ApiResponseError> getResponseEntity(Long error, HttpStatus httpStatus, String failReason) {
        ApiResponseError responseError = new ApiResponseError(error, failReason);
        return new ResponseEntity<>(responseError, httpStatus);
    }
}
