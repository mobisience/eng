package pl.wiacekp.exceptions;

import lombok.Getter;

@Getter
public class WordExceptions extends Exception {
	private static final long serialVersionUID = 1L;

	public WordExceptions(FailReason failReason) {
        this.failReason = failReason;
    }

    public enum FailReason {CARD_NOT_FOUND, UNATHORIZE}

    private FailReason failReason;
}