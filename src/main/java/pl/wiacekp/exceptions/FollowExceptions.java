package pl.wiacekp.exceptions;

import lombok.Getter;

@Getter
public class FollowExceptions extends Exception {
	private static final long serialVersionUID = 1L;

	public FollowExceptions(FailReason failReason) {
        this.failReason = failReason;
    }

    public enum FailReason {DECK_NOT_FOUND, UNAUTHORIZE, FOLLOW_NOT_FOUND}

    private FailReason failReason;
}