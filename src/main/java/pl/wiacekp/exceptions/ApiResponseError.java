package pl.wiacekp.exceptions;

import lombok.Getter;

@Getter
public class ApiResponseError {
    public ApiResponseError(Long httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    private Long httpStatus;
    private String message;
}
