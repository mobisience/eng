package pl.wiacekp.exceptions;

import lombok.Getter;

@Getter
public class DeckExceptions extends Exception {
	private static final long serialVersionUID = 1L;

	public DeckExceptions(FailReason failReason) {
        this.failReason = failReason;
    }

    public enum FailReason {UNAUTHORIZE}

    private FailReason failReason;
}
