package pl.wiacekp.exceptions;

import lombok.Getter;

/**
 * Created by Piotrek on 2017-01-15.
 */
@Getter
public class ResultExceptions extends Exception {
    private static final long serialVersionUID = 1L;

    public ResultExceptions(FailReason failReason) {
        this.failReason = failReason;
    }

    public enum FailReason {UNAUTHORIZE, CARD_NOT_FOUND, DECK_NOT_FOLLOWED, DECK_NOT_FOUND}

    private FailReason failReason;
}
