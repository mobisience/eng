package pl.wiacekp.exceptions;

import lombok.Getter;

@Getter
public class CardExceptions extends Exception {
	private static final long serialVersionUID = 1L;

	public CardExceptions(FailReason failReason) {
        this.failReason = failReason;
    }

    public enum FailReason {UNAUTHORIZE, PARAMETERS_NOT_CORRECT, DECK_NOT_FOUND}

    private FailReason failReason;
}