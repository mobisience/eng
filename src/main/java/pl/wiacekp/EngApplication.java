package pl.wiacekp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@ComponentScan("pl.wiacekp")
public class EngApplication {

	public static void main(String[] args) {
		SpringApplication.run(EngApplication.class, args);
	}
}
