package pl.wiacekp.domain;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import pl.wiacekp.view.Views;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by pwiacek on 10/11/2016.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Result {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonView(Views.Results.class)
    private Account user;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonView(Views.Results.class)
    private Card card;

    @NotNull
    @JsonView(Views.Public.class)
    private LocalDateTime date;

    @NotNull
    @JsonView(Views.Public.class)
    private ResultType resultType;

    @NotNull
    @JsonView(Views.Public.class)
    private LocalDateTime remind;

    @NotNull
    @JsonView(Views.Public.class)
    private Remind remindType;
}
