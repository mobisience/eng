package pl.wiacekp.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.wiacekp.view.Views;


/**
 * Created by pwiacek on 10/11/2016.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Account {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private Long id;

    @NotNull
    @JsonView(Views.Public.class)
    private String name;

    @NotNull
    @JsonView(Views.Public.class)
    private String surname;

    @NotNull
    @JsonView(Views.Public.class)
    private String login;

    @NotNull
    @JsonView(Views.Public.class)
    private String email;

    @NotNull
    @JsonView(Views.Private.class)
    private String pass;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JsonView(Views.Private.class)
    private List<Role> roles;
}
