package pl.wiacekp.domain;

/**
 * Created by Piotrek on 2017-01-15.
 */
public enum Remind {
    MIN2, MIN10, HOUR1, DAY1, DAY2, WEEK1, MONTH1, MONTH6, YEAR1
}
