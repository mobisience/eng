package pl.wiacekp.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.wiacekp.view.Views;


/**
 * Created by pwiacek on 10/11/2016.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Card {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonView(Views.Cards.class)
    private Deck deck;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonView(Views.Cards.class)
    private Word fWord;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonView(Views.Cards.class)
    private Word sWord;
}
