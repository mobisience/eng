package pl.wiacekp.domain;

/**
 * Created by pwiacek on 10/11/2016.
 */
public enum Lang {
    EN, PL
}
