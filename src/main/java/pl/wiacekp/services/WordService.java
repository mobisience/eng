package pl.wiacekp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.wiacekp.domain.Card;
import pl.wiacekp.domain.Word;
import pl.wiacekp.exceptions.WordExceptions;
import pl.wiacekp.exceptions.WordExceptions.FailReason;
import pl.wiacekp.repositories.CardsRepository;
import pl.wiacekp.repositories.WordRepository;

/**
 * Created by pwiacek on 10/11/2016.
 */
@Service
public class WordService {
    private WordRepository wordRepository;
    private CardsRepository cardRepository;
    private AccountService accountService;

    @Autowired
    public WordService(WordRepository wordRepository, CardsRepository cardRepository, AccountService accountService) {
        this.wordRepository = wordRepository;
        this.cardRepository = cardRepository;
        this.accountService = accountService;
    }

    public Word addWord(Word word){
        return wordRepository.save(word);
    }
    
    public Word updateWord(Word word, Long wordId) throws WordExceptions{
    	Word currentWord = wordRepository.findOne(wordId);
    	Iterable<Card> list = cardRepository.findAllByFWord(currentWord);
    	if(!list.iterator().hasNext()){
    		list = cardRepository.findAllBySWord(currentWord);
    		if(!list.iterator().hasNext()){
    			throw new WordExceptions(FailReason.CARD_NOT_FOUND);
    		}
    	}
    	
    	Card card = list.iterator().next();
    	if(!card.getDeck().getAccount().getId().equals(accountService.getMyAccount().getId())){
			throw new WordExceptions(FailReason.UNATHORIZE);
    	}
    	
    	word.setId(wordId);
    	return wordRepository.save(word);
    }

    public Iterable<Word> getWords(){
        return wordRepository.findAll();
    }

    public Word getOne(Long id){
        return wordRepository.findOne(id);
    }

    public void deleteWord(Long id){
        wordRepository.delete(id);
    }
}
