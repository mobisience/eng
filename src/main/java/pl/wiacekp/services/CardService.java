package pl.wiacekp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.wiacekp.domain.Card;
import pl.wiacekp.domain.Count;
import pl.wiacekp.domain.Deck;
import pl.wiacekp.exceptions.CardExceptions;
import pl.wiacekp.exceptions.CardExceptions.FailReason;
import pl.wiacekp.exceptions.FollowExceptions;
import pl.wiacekp.repositories.CardsRepository;
import pl.wiacekp.repositories.DeckRepository;

/**
 * Created by pwiacek on 10/11/2016.
 */
@Service
public class CardService {
    private CardsRepository cardsRepository;
    private DeckRepository deckRepository;
    private AccountService accountService;

    @Autowired
    public CardService(CardsRepository cardsRepository, DeckRepository deckRepository, AccountService accountService) {
        this.cardsRepository = cardsRepository;
        this.deckRepository = deckRepository;
        this.accountService = accountService;
    }

    public Iterable<Card> getCardsByDeckID(Long deckId) {
        Deck deck = deckRepository.findOne(deckId);
        return cardsRepository.findAllByDeck(deck);
    }

    public Iterable<Card> findAllNewCardByUserAndDeck(Long deckID) throws CardExceptions {
        Deck deck = deckRepository.findOne(deckID);
        if (deck == null) {
            throw new CardExceptions(FailReason.DECK_NOT_FOUND);
        }
        return cardsRepository.findAllNewDeckByDeckAndUser(deck, accountService.getMyAccount());
    }

    public Count getCountNewCardByUserAndDeck(Long deckID) throws CardExceptions {
        Deck deck = deckRepository.findOne(deckID);
        if (deck == null) {
            throw new CardExceptions(FailReason.DECK_NOT_FOUND);
        }
        return new Count(cardsRepository.countNewDeckByDeckAndUser(deck, accountService.getMyAccount()));
    }

    public Card addCard(Card card) {
        return cardsRepository.save(card);
    }

    public Card updateCard(Long cardId, Card card) throws CardExceptions {
        Card currentCard = cardsRepository.findOne(cardId);
        if (!currentCard.getDeck().getAccount().getId().equals(accountService.getMyAccount().getId())) {
            throw new CardExceptions(FailReason.UNAUTHORIZE);
        }
        if (!currentCard.getDeck().getId().equals(card.getDeck().getId())) {
            throw new CardExceptions(FailReason.PARAMETERS_NOT_CORRECT);
        }
        card.setId(cardId);
        card.setDeck(deckRepository.getOne(currentCard.getDeck().getId()));
        return cardsRepository.save(card);
    }

    public Count getCountCardsByDeckId(Long deckId) throws CardExceptions {
        Deck deck = deckRepository.findOne(deckId);
        if (deck == null) {
            throw new CardExceptions(CardExceptions.FailReason.DECK_NOT_FOUND);
        }

        return new Count(cardsRepository.countByDeck(deck));
    }

    public List<Card> getCards() {
        return cardsRepository.findAll();
    }

    public Card getOne(Long id) {
        return cardsRepository.findOne(id);
    }

    public void deleteCard(Long id) {
        cardsRepository.delete(id);
    }
}
