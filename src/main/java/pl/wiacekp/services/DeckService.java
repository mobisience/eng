package pl.wiacekp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import pl.wiacekp.domain.Account;
import pl.wiacekp.domain.Deck;
import pl.wiacekp.domain.Follow;
import pl.wiacekp.exceptions.DeckExceptions;
import pl.wiacekp.exceptions.DeckExceptions.FailReason;
import pl.wiacekp.exceptions.FollowExceptions;
import pl.wiacekp.repositories.DeckRepository;

/**
 * Created by pwiacek on 10/11/2016.
 */
@Service
public class DeckService {
    private DeckRepository deckRepository;
    private AccountService accountService;
    private FollowService followService;

    @Autowired
    public DeckService(DeckRepository deckRepository, AccountService accountService, FollowService followService) {
        this.deckRepository = deckRepository;
        this.accountService = accountService;
        this.followService = followService;
    }

    public Deck addDeck(Deck deck) {
        deck.setAccount(accountService.getMyAccount());
        Deck savedDeck = deckRepository.save(deck);
        followService.addFollow(Follow.builder()
                .user(accountService.getMyAccount())
                .deck(savedDeck)
                .startWithFWord(true).build());
        return savedDeck;
    }

    public Deck updateDeck(Deck deck, Long deckId) throws DeckExceptions {
        Deck currentDeck = getOne(deckId);

        if (!currentDeck.getAccount().getId().equals(accountService.getMyAccount().getId())) {
            throw new DeckExceptions(FailReason.UNAUTHORIZE);
        }
        deck.setAccount(accountService.getMyAccount());
        deck.setId(deckId);

        return deckRepository.save(deck);
    }

    public Iterable<Deck> getDecksByOwner() {
        Account account = accountService.getMyAccount();
        return deckRepository.findAllByAccount(account);
    }

    public Iterable<Deck> getDecks(Integer page, Integer size) {
        if (page == null || size == null) {
            return getDecks();
        }
        Pageable pageable = new PageRequest(page, size);
        return deckRepository.findAll(pageable);
    }

    public Iterable<Deck> getDecks() {
        return deckRepository.findAll();
    }

    public Deck getOne(Long id) {
        return deckRepository.findOne(id);
    }

    public void deleteDeck(Long id) throws DeckExceptions, FollowExceptions {
        Account account = accountService.getMyAccount();
        Deck deck = getOne(id);
        if (!account.getId().equals(deck.getAccount().getId())) {
            throw new DeckExceptions(FailReason.UNAUTHORIZE);
        }
        followService.findAllByDeck(id).forEach(c -> followService.deleteFollow(c.getId()));
        deckRepository.delete(id);
    }
}
