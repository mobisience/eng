package pl.wiacekp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.wiacekp.domain.Account;
import pl.wiacekp.domain.Deck;
import pl.wiacekp.domain.Follow;
import pl.wiacekp.repositories.AccountRepository;
import pl.wiacekp.repositories.DeckRepository;
import pl.wiacekp.repositories.FollowRepository;
import pl.wiacekp.security.PasswordEncoderService;

/**
 * Created by pwiacek on 10/11/2016.
 */
@Service
public class AccountService {
    private AccountRepository accountRepository;
    private FollowRepository followRepository;
    private DeckRepository deckRepository;
	private PasswordEncoderService passwordEncoderService;

	@Autowired
    public AccountService(AccountRepository accountRepository, FollowRepository followRepository, DeckRepository deckRepository, PasswordEncoderService passwordEncoderService) {
        this.accountRepository = accountRepository;
        this.followRepository = followRepository;
        this.deckRepository = deckRepository;
        this.passwordEncoderService = passwordEncoderService;
    }

    @Value(value = "${follow.add_admins_deck}")
    private boolean addAdminDecks;
    @Value(value = "${follow.admin_id}")
    private Long adminId;

    public Account addAccount(Account user){
    	PasswordEncoder pe = passwordEncoderService.passwordEncoder();
    	user.setPass(pe.encode(user.getPass()));
        Account account = accountRepository.save(user);

        if(addAdminDecks){
            Account newUserAccount = accountRepository.findOne(account.getId());
            Iterable<Deck> allByAccount = deckRepository.findAllByAccount(accountRepository.findOne(adminId));
            for(Deck deck: allByAccount){
                Follow follow = Follow.builder().user(newUserAccount).startWithFWord(true).deck(deck).build();
                followRepository.save(follow);
            }
        }

        return account;
    }
    
    public Account getMyAccount(){
		return accountRepository.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public Iterable<Account> getAccounts(){
        return accountRepository.findAll();
    }

    public Account getOne(Long id){
        return accountRepository.findOne(id);
    }

    public void deleteAccount(Long id){
        accountRepository.delete(id);
    }
}
