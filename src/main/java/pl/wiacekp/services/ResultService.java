package pl.wiacekp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wiacekp.domain.*;
import pl.wiacekp.exceptions.ResultExceptions;
import pl.wiacekp.repositories.CardsRepository;
import pl.wiacekp.repositories.DeckRepository;
import pl.wiacekp.repositories.FollowRepository;
import pl.wiacekp.repositories.ResultRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pwiacek on 10/11/2016.
 */
@Service
public class ResultService {
    private ResultRepository resultRepository;
    private AccountService accountService;
    private DeckRepository deckRepository;
    private CardsRepository cardsRepository;
    private FollowRepository followRepository;

    @Autowired
    public ResultService(ResultRepository resultRepository, AccountService accountService, DeckRepository deckRepository, CardsRepository cardsRepository, FollowRepository followRepository) {
        this.resultRepository = resultRepository;
        this.accountService = accountService;
        this.deckRepository = deckRepository;
        this.cardsRepository = cardsRepository;
        this.followRepository = followRepository;
    }

    public Card nextCard(Long deckId, Result result) throws ResultExceptions {
        Account myAccount = accountService.getMyAccount();
        if (myAccount == null) {
            throw new ResultExceptions(ResultExceptions.FailReason.UNAUTHORIZE);
        }
        boolean forceNextWord = (result != null && result.getCard() != null) ? false : true;

        Deck deck = deckRepository.findOne(deckId);
        if (deck == null) {
            throw new ResultExceptions(ResultExceptions.FailReason.DECK_NOT_FOUND);
        }

        if (!followRepository.findAllByUserAndDeck(myAccount, deck).iterator().hasNext()) {
            throw new ResultExceptions(ResultExceptions.FailReason.DECK_NOT_FOLLOWED);
        }

        //save new result
        if (!forceNextWord) {
            Card card = cardsRepository.findOne(result.getCard().getId());
            if (card == null) {
                throw new ResultExceptions(ResultExceptions.FailReason.CARD_NOT_FOUND);
            }

            Iterable<Result> allByUserAndCard = resultRepository.findAllByUserAndCard(myAccount, card);
            resultRepository.delete(allByUserAndCard);

            Remind[] reminds = Remind.values();
            Remind newRemind = reminds[0];

            if (!allByUserAndCard.iterator().hasNext() && (result.getResultType() == ResultType.EASY || result.getResultType() == ResultType.HARD)) {
                newRemind = Remind.MIN10;
            }
            if (allByUserAndCard.iterator().hasNext()) {
                Result next = allByUserAndCard.iterator().next();
                int index = next.getResultType().ordinal();
                switch (next.getResultType()) {
                    case HARD:
                        newRemind = next.getRemindType();
                        break;
                    case EASY:
                        newRemind = (index + 1 < reminds.length) ? reminds[index + 1] : reminds[reminds.length - 1];
                        break;
                }
            }

            Result build = Result.builder()
                    .user(myAccount)
                    .card(card)
                    .date(LocalDateTime.now())
                    .resultType(result.getResultType())
                    .remindType(newRemind)
                    .remind(getNewDateByRemind(LocalDateTime.now(), newRemind)).build();

            resultRepository.save(build);
        }

        //get rev
        for (Result element : resultRepository.findAllByUserAndRemindAfterToday(myAccount, LocalDateTime.now())) {
            if (element.getCard().getDeck().getId().equals(deck.getId()))
                return element.getCard();
        }

        //get new
        Iterable<Card> allNewDeckByDeckAndUser = cardsRepository.findAllNewDeckByDeckAndUser(deck, accountService.getMyAccount());
        if (allNewDeckByDeckAndUser.iterator().hasNext())
            return allNewDeckByDeckAndUser.iterator().next();

        //next card not found
        return null;
    }

    public Iterable<Result> getResults() {
        return resultRepository.findAll();
    }

    public Result getOne(Long id) {
        return resultRepository.findOne(id);
    }

    public void deleteResult(Long id) {
        resultRepository.delete(id);
    }

    public List<Result> getAllRevResultsByDeck(Long deckId) {
        return getAllRevResultByDeckCollection(deckId);
    }

    public Count getCountRevResultsByDeck(Long deckId) {
        return new Count((long) getAllRevResultByDeckCollection(deckId).size());
    }

    List<Result> getAllRevResultByDeckCollection(Long deckId) {   //todo: move this to repository
        Account myAccount = accountService.getMyAccount();
        Deck deck = deckRepository.getOne(deckId);
        List<Result> list = new ArrayList<>();
        resultRepository.findAllByUserAndRemindAfterToday(myAccount, LocalDateTime.now())
                .forEach(c -> list.add(c));
        List<Result> collect = list.stream()
                .filter(c -> c.getCard().getDeck().getId().equals(deck.getId()))
                .collect(Collectors.toList());
        return collect;
    }

    private LocalDateTime getNewDateByRemind(LocalDateTime date, Remind remind) {
        switch (remind) {
            case MIN2:
                return date.plusMinutes(2L);
            case MIN10:
                return date.plusMinutes(10L);
            case HOUR1:
                return date.plusHours(1L);
            case DAY1:
                return date.plusDays(1L);
            case DAY2:
                return date.plusDays(2L);
            case WEEK1:
                return date.plusWeeks(1L);
            case MONTH1:
                return date.plusMonths(1L);
            case MONTH6:
                return date.plusMonths(6L);
            case YEAR1:
                return date.plusYears(1L);
            default:
                return date;
        }
    }
}
