package pl.wiacekp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.wiacekp.domain.Count;
import pl.wiacekp.domain.Deck;
import pl.wiacekp.domain.Follow;
import pl.wiacekp.exceptions.FollowExceptions;
import pl.wiacekp.exceptions.FollowExceptions.FailReason;
import pl.wiacekp.repositories.DeckRepository;
import pl.wiacekp.repositories.FollowRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pwiacek on 10/11/2016.
 */
@Service
public class FollowService {
    private FollowRepository followRepository;
    private DeckRepository deckRepository;
    private AccountService accountService;

    @Autowired
    public FollowService(FollowRepository followRepository, DeckRepository deckRepositor, AccountService accountService) {
        this.followRepository = followRepository;
        this.deckRepository = deckRepositor;
        this.accountService = accountService;
    }

    public Follow addFollow(Follow follow) {
        follow.setUser(accountService.getMyAccount());
        return followRepository.save(follow);
    }


    public Iterable<Follow> getFollows() {
        return followRepository.findAll();
    }

    public Follow getOne(Long id) {
        return followRepository.findOne(id);
    }

    public Follow updateFollow(Follow follow, Long followId) throws FollowExceptions {
        Follow currentFollow = followRepository.findOne(followId);
        if (currentFollow == null) {
            throw new FollowExceptions(FailReason.FOLLOW_NOT_FOUND);
        }
        if (!currentFollow.getUser().getId().equals(accountService.getMyAccount().getId())) {
            throw new FollowExceptions(FailReason.UNAUTHORIZE);
        }
        follow.setUser(accountService.getMyAccount());
        follow.setId(followId);

        return followRepository.save(follow);
    }

    public Iterable<Follow> getMyFollows() {
        return followRepository.findAllByUser(accountService.getMyAccount());
    }

    public Follow getMyFollowsByDeck(long deckId) {
        List<Follow> list = new ArrayList<>();
        followRepository
                .findAllByUserAndDeck(accountService.getMyAccount(), deckRepository.findOne(deckId))
                .forEach(c -> list.add(c));
        Follow follow = null;

        if (list.size() == 1) {
            follow = list.get(0);
        } else if (list.size() > 1) {
            follow = list.get(0);
            list.remove(0);
            list.stream().forEach(c -> followRepository.delete(c.getId()));
        }

        return follow;
    }

    public Iterable<Follow> findAllByDeck(Long deckID) throws FollowExceptions {
        Deck deck = deckRepository.findOne(deckID);
        if (deck == null) {
            throw new FollowExceptions(FailReason.DECK_NOT_FOUND);
        }

        return followRepository.findAllByDeck(deck);
    }

    public Count getCountByDeck(Long deckId) throws FollowExceptions {
        Deck deck = deckRepository.findOne(deckId);
        if (deck == null) {
            throw new FollowExceptions(FailReason.DECK_NOT_FOUND);
        }

        return new Count(followRepository.countByDeck(deck));
    }

    public void deleteFollow(Long id) {
        followRepository.delete(id);
    }
}
