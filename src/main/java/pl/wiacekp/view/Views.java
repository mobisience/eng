package pl.wiacekp.view;

public class Views {
	public static class Public {
	}

	public static class Cards extends Public {
	}

	public static class Private extends Public {
	}

	public static class Decks extends Public {
	}

	public static class Follows extends Public {
	}

	public static class Positions extends Public {
	}

	public static class Results extends Public {
	}

	public static class Accounts extends Public {
	}

	public static class Words extends Public {
	}
}
