package pl.wiacekp.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.wiacekp.domain.Account;
import pl.wiacekp.domain.Role;
import pl.wiacekp.repositories.AccountRepository;


@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Account account = accountRepository.findByLogin(login);
        List<GrantedAuthority> authorities = buildUserAuthority(account.getRoles());
        return buildUserForAuthentication(account, authorities);
    }

    private UserDetails buildUserForAuthentication(Account account, List<GrantedAuthority> authorities) {
        return new User(account.getLogin(), account.getPass(), true, true, true, true, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(List<Role> roles) {
        List<GrantedAuthority> authorityList = new ArrayList<GrantedAuthority>();

        for (Role role : roles) {
            authorityList.add(new SimpleGrantedAuthority(role.getRole()));
        }

        return authorityList;
    }


}
