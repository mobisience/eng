package pl.wiacekp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.wiacekp.domain.Account;
import pl.wiacekp.domain.Deck;

/**
 * Created by pwiacek on 10/11/2016.
 */
public interface DeckRepository extends JpaRepository<Deck, Long> {
	Iterable<Deck> findAllByAccount(Account account);
}
