package pl.wiacekp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.wiacekp.domain.Word;

/**
 * Created by pwiacek on 10/11/2016.
 */
public interface WordRepository extends JpaRepository<Word, Long> {

}
