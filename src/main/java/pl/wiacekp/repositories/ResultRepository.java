package pl.wiacekp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wiacekp.domain.Account;
import pl.wiacekp.domain.Card;
import pl.wiacekp.domain.Result;

import java.time.LocalDateTime;

/**
 * Created by pwiacek on 10/11/2016.
 */
public interface ResultRepository extends JpaRepository<Result, Long> {
    Iterable<Result> findAllByUser(Account user);
    Iterable<Result> findAllByUserAndCard(Account user, Card card);

    @Query("SELECT p FROM Result p WHERE p.user = :user and p.remind < :today")
    Iterable<Result> findAllByUserAndRemindAfterToday(@Param("user") Account user, @Param("today") LocalDateTime today);
}