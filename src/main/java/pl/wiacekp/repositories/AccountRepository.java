package pl.wiacekp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.wiacekp.domain.Account;

/**
 * Created by pwiacek on 10/11/2016.
 */
public interface AccountRepository extends JpaRepository<Account, Long> {
	Account findByLogin(String login);
}