package pl.wiacekp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.wiacekp.domain.Account;
import pl.wiacekp.domain.Deck;
import pl.wiacekp.domain.Follow;

/**
 * Created by pwiacek on 10/11/2016.
 */
public interface FollowRepository extends JpaRepository<Follow, Long> {
	Iterable<Follow> findAllByDeck(Deck deck);
	Iterable<Follow> findAllByUser(Account user);
	Iterable<Follow> findAllByUserAndDeck(Account user, Deck deck);
	Long countByDeck(Deck deck);
}
