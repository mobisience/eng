package pl.wiacekp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wiacekp.domain.Account;
import pl.wiacekp.domain.Card;
import pl.wiacekp.domain.Deck;
import pl.wiacekp.domain.Word;

/**
 * Created by pwiacek on 10/11/2016.
 */
public interface CardsRepository extends JpaRepository<Card, Long> {
	Iterable<Card> findAllByDeck(Deck deck);
	Iterable<Card> findAllByFWord(Word fWord);
	Iterable<Card> findAllBySWord(Word sWord);
	Card getOne(Long id);
	Long countByDeck(Deck deck);

	@Query("SELECT a FROM Card a WHERE a.deck = :deck AND a NOT IN (SELECT c.card from Result c where c.card = a and c.user = :user)")
	Iterable<Card> findAllNewDeckByDeckAndUser(@Param("deck") Deck deck, @Param("user") Account user);

	@Query("SELECT count(a) FROM Card a WHERE a.deck = :deck AND a NOT IN (SELECT c.card from Result c where c.card = a and c.user = :user)")
	Long countNewDeckByDeckAndUser(@Param("deck") Deck deck, @Param("user") Account user);
}
