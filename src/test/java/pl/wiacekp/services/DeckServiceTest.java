package pl.wiacekp.services;

import static org.mockito.Mockito.doReturn;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import pl.wiacekp.domain.Deck;
import pl.wiacekp.domain.Account;
import pl.wiacekp.repositories.DeckRepository;

import lombok.*;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class DeckServiceTest {
	@Mock
	private DeckRepository deckRepository;

	@InjectMocks
	private DeckService deckService;
	
	private Deck deck;
	private Account user;
	
	@Before
	public void setUp(){
		user = Account.builder()
				.id(1L)
				.name("UserName")
				.surname("UserSurname")
				.email("usermail@mail.com")
				.pass("secret")
				.login("myLogin").build();
		
		deck = Deck.builder()
				.id(1L)
				.description("description")
				.account(user)
				.name("MyDeck")
				.build();
	}
	
	@Test
	public void addDeckTest(){
		Deck deckToAdd = deck;
		deckToAdd.setId(null);
		
		doReturn(deck).when(deckRepository).save(deckToAdd);
		
		Deck returnedDeck = deckService.addDeck(deckToAdd);
		Assert.assertEquals(deck, returnedDeck);
		
		deckToAdd.setAccount(null);
		
		returnedDeck = deckService.addDeck(deckToAdd);
		Assert.assertEquals(deck, returnedDeck);
	}
	
	@Test
	public void getDecksTest(){
		
	}
	
	@Test
	public void getOneTest(){
		
	}
	
	@Test
	public void deleteDeckTest(){
		
	}
}
